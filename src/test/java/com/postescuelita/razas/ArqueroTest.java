package com.postescuelita.razas;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ArqueroTest {

    private static final int ATAQUE_BASICO_ARQUERO = 15;
    private static final int SALUD_MAXIMA_ARQUERO = 50;
    private static final int DEFENZA_ARQUERO = 10;
    private static final int ATAQUE_BASICO_VAINILLA = 10;
    private static final int SALUD_MAXIMA_VAINILLA = 100;
    Personaje arquero;
    Personaje vainilla;

    @BeforeEach
    void setUp() {
        arquero = new Arquero("Robin Hood", SALUD_MAXIMA_ARQUERO, ATAQUE_BASICO_ARQUERO, DEFENZA_ARQUERO, 2);
        vainilla = new Vainilla("Azucar", SALUD_MAXIMA_VAINILLA, ATAQUE_BASICO_VAINILLA, 0);
    }

    @Test
    void recibirAtaque_conAtaqueVainilla_descontarVida() {
        for (int i = 1; i <= 10; i++) {
            vainilla.atacar(arquero);
            assertEquals(SALUD_MAXIMA_ARQUERO - (ATAQUE_BASICO_VAINILLA / 2) * i, arquero.getSalud());
        }
        vainilla.atacar(arquero);
        assertEquals(0, arquero.getSalud());
        vainilla.atacar(arquero);
        assertEquals(0, arquero.getSalud());
    }

    @Test
    void atacar_atacandoVainilla_descuentaCorrectamenteSaludVainilla() {
        for (int i = 1; i <= 6; i++) {
            arquero.atacar(vainilla);
            assertEquals(SALUD_MAXIMA_VAINILLA + (-ATAQUE_BASICO_ARQUERO) * i, vainilla.getSalud());
        }
        arquero.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
        arquero.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
    }

}
