package com.postescuelita.razas;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MagoTest {

    private static final int ATAQUE_BASICO_MAGO = 10;
    private static final int SALUD_MAXIMA_MAGO = 75;
    private static final int DEFENZA_MAGO = 5;
    private static final int ESCUDO_MAGO = 15;
    private static final int ATAQUE_BASICO_VAINILLA = 10;
    private static final int SALUD_MAXIMA_VAINILLA = 100;

    Personaje mago;
    Personaje vainilla;

    @BeforeEach
    void setUp() {
        mago = new Mago("Merlín", SALUD_MAXIMA_MAGO, ATAQUE_BASICO_MAGO, DEFENZA_MAGO, ESCUDO_MAGO);
        vainilla = new Vainilla("Azucar", SALUD_MAXIMA_VAINILLA, ATAQUE_BASICO_VAINILLA, 0);
    }

    @Test
    void recibirAtaque_conAtaqueVainilla_descontarVida() {
        vainilla.atacar(mago);
        assertEquals(SALUD_MAXIMA_MAGO, mago.getSalud());
        assertEquals(5, mago.getEspecial());
        vainilla.atacar(mago);
        assertEquals(SALUD_MAXIMA_MAGO - (ATAQUE_BASICO_VAINILLA - DEFENZA_MAGO) + 5, mago.getSalud());
        assertEquals(0, mago.getEspecial());
        for (int i = 1; i <= 14; i++) {
            vainilla.atacar(mago);
            assertEquals(SALUD_MAXIMA_MAGO - (ATAQUE_BASICO_VAINILLA - DEFENZA_MAGO) * i, mago.getSalud());
        }
        vainilla.atacar(mago);
        assertEquals(0, mago.getSalud());
        vainilla.atacar(mago);
        assertEquals(0, mago.getSalud());
    }

    @Test
    void atacar_atacandoVainilla_descuentaCorrectamenteSaludVainilla() {
        for (int i = 1; i <= 10; i++) {
            mago.atacar(vainilla);
            assertEquals(SALUD_MAXIMA_VAINILLA + (-ATAQUE_BASICO_MAGO) * i, vainilla.getSalud());
        }
        mago.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
        mago.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
    }

    @Test
    void recibirAtaque_atacandoVainilla_descontarEscudo() {
        vainilla.atacar(mago);
        assertEquals(SALUD_MAXIMA_MAGO, mago.getSalud());
        assertEquals(5, mago.getEspecial());
        vainilla.atacar(mago);
        assertEquals(SALUD_MAXIMA_MAGO - (ATAQUE_BASICO_VAINILLA - DEFENZA_MAGO) + 5, mago.getSalud());
        assertEquals(0, mago.getEspecial());
        vainilla.atacar(mago);
        assertEquals(0, mago.getEspecial());
    }

}
