package com.postescuelita.razas;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GuerreroTest {

    private static final int ATAQUE_BASICO_GUERRERO = 5;
    private static final int SALUD_MAXIMA_GUERRERO = 100;
    private static final int DEFENZA_GUERRERO = 5;
    private static final int ATAQUE_BASICO_VAINILLA = 10;
    private static final int SALUD_MAXIMA_VAINILLA = 100;
    private static final int ADRENALINA_GUERRERO = 3;
    Personaje guerrero;
    Personaje vainilla;

    @BeforeEach
    void setUp() {
        guerrero = new Guerrero("Leónidas", SALUD_MAXIMA_GUERRERO, ATAQUE_BASICO_GUERRERO, DEFENZA_GUERRERO, ADRENALINA_GUERRERO);
        vainilla = new Vainilla("Azucar", SALUD_MAXIMA_VAINILLA, ATAQUE_BASICO_VAINILLA, 0);
    }

    @Test
    void recibirAtaque_conAtaqueVainilla_descontarVida() {
        for (int i = 1; i <= 19; i++) {
            vainilla.atacar(guerrero);
            assertEquals(SALUD_MAXIMA_GUERRERO - (ATAQUE_BASICO_VAINILLA - DEFENZA_GUERRERO) * i, guerrero.getSalud());
        }
        vainilla.atacar(guerrero);
        assertEquals(0, guerrero.getSalud());
        vainilla.atacar(guerrero);
        assertEquals(0, guerrero.getSalud());
    }

    @Test
    void atacar_atacandoVainilla_descuentaCorrectamenteSaludVainilla() {
        for (int i = 1; i <= 6; i++) {
            guerrero.atacar(vainilla);
            assertEquals(SALUD_MAXIMA_VAINILLA + (-ATAQUE_BASICO_GUERRERO * ADRENALINA_GUERRERO) * i, vainilla.getSalud());
        }
        guerrero.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
        guerrero.atacar(vainilla);
        assertEquals(0, vainilla.getSalud());
    }

    @Test
    void recibirAtaque_atacandoVainilla_disminuyeAdrenalina() {
        assertEquals(3, guerrero.getEspecial());
        vainilla.atacar(guerrero);
        assertEquals(2, guerrero.getEspecial());
        vainilla.atacar(guerrero);
        assertEquals(1, guerrero.getEspecial());
        vainilla.atacar(guerrero);
        assertEquals(0, guerrero.getEspecial());
        vainilla.atacar(guerrero);
        assertEquals(0, guerrero.getEspecial());
    }

    @Test
    void recibirAtaque_atacandoVainilla_seMultiplicaAtaque() {
        assertEquals(5, guerrero.getAtaque());
        vainilla.atacar(guerrero);
        assertEquals(15, guerrero.getAtaque());
        vainilla.atacar(guerrero);
        assertEquals(30, guerrero.getAtaque());
        vainilla.atacar(guerrero);
        assertEquals(30, guerrero.getAtaque());
        vainilla.atacar(guerrero);
        assertEquals(30, guerrero.getAtaque());
    }

}
