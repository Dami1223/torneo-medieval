package com.postescuelita.torneo;

import com.postescuelita.razas.Arquero;
import com.postescuelita.razas.Guerrero;
import com.postescuelita.razas.Mago;
import com.postescuelita.razas.Personaje;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TorneoTest {

    Torneo torneo;
    List<Personaje> participantes;
    Personaje ganador;

    @BeforeEach
    void setUp() {
        participantes = new LinkedList<>();
        participantes.add(new Mago("Merlín", 75, 10, 5, 15));
        participantes.add(new Arquero("Robin Hood", 50, 15, 10, 2));
        participantes.add(new Guerrero("Leónidas", 100, 5, 5, 3));
        torneo = new Torneo(participantes);
    }

    @Test
    void iniciarCombates_conParticipantes_ganadorLeonidas() {
        ganador = participantes.get(2);
        torneo.iniciarCombates();
        Assertions.assertEquals(1,torneo.getParticipantes().size());
        Assertions.assertEquals(ganador,torneo.getParticipantes().get(0));
    }

}
