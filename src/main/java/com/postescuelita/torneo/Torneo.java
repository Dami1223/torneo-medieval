package com.postescuelita.torneo;

import java.util.List;

import com.postescuelita.razas.Personaje;

public class Torneo {

    private List<Personaje> participantes;

    public Torneo(List<Personaje> participantes) {
        this.participantes = participantes;
    }

    public void iniciarCombates() {
        int j = 1;
        while (participantes.size() > 1) {
            ronda(j);
            j++;
        }
    }

    private void ronda(int j) {
        System.out.println("Ronda n°" + j);
        participantes.sort(null);
        Personaje atacante = participantes.get(0);
        for (int i = participantes.size() - 1; i > 0; i--) {
            System.out.println("El personaje: " + atacante + " atacará a: " + participantes.get(i));
            atacante.atacar(participantes.get(i));
            if (participantes.get(i).getSalud() == 0) {
                System.out.println("El personaje: " + participantes.get(i) + " no puede continuar!");
                participantes.remove(participantes.get(i));
            }
        }
    }

    public List<Personaje> getParticipantes() {
        return participantes;
    }
}
