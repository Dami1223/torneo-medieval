package com.postescuelita.razas;

public class Arquero extends Personaje {

    private int evacion;

    public Arquero(String nombre, int salud, int ataque, int defensa, int evacion) {
        this.ataque = ataque;
        this.defensa = defensa;
        this.nombre = nombre;
        this.salud = salud;
        this.evacion = evacion;
    }

    @Override
    public void atacar(Personaje oponente) {
        oponente.recibirAtaque(ataque);
    }

    @Override
    public void recibirAtaque(int ataque) {
        this.salud -= ataque / evacion;
        if (this.salud < 0) {
            this.salud = 0;
        }
    }

    @Override
    public int getEspecial() {
        return this.evacion;
    }

}
