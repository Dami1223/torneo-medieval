package com.postescuelita.razas;

public class Mago extends Personaje {

    private int escudo;

    public Mago(String nombre, int salud, int ataque, int defensa, int escudo) {
        this.ataque = ataque;
        this.defensa = defensa;
        this.nombre = nombre;
        this.salud = salud;
        this.escudo = escudo;
    }

    @Override
    public void atacar(Personaje oponente) {
        oponente.recibirAtaque(this.ataque);
    }

    @Override
    public void recibirAtaque(int ataque) {
        if (ataque > this.escudo) {
            this.salud -= ataque - this.escudo - this.defensa;
            this.escudo = 0;
            if (this.salud < 0) {
                this.salud = 0;
            }
        } else {
            this.escudo -= ataque;
        }
    }

    @Override
    public int getEspecial() {
        return this.escudo;
    }

    @Override
    public String toString() {
        return super.toString() + "[Escudo=" + escudo + "]";
    }

}
