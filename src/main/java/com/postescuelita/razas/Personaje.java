package com.postescuelita.razas;

public abstract class Personaje implements Comparable<Personaje> {

    protected String nombre;
    protected int salud;
    protected int ataque;
    protected int defensa;

    public abstract void atacar(Personaje oponente);

    public abstract void recibirAtaque(int ataque);

    public String getNombre() {
        return nombre;
    }

    public int getSalud() {
        return salud;
    }

    public int getAtaque() {
        return ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public abstract int getEspecial();

    @Override
    public int compareTo(Personaje otro) {
        return Integer.compare(this.salud, otro.salud);
    }

    @Override
    public String toString() {
        return nombre + " [HP=" + salud + "]";
    }

}
