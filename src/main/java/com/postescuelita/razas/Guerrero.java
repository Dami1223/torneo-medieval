package com.postescuelita.razas;

public class Guerrero extends Personaje {

    private int adrenalina;

    public Guerrero(String nombre, int salud, int ataque, int defensa, int adrenalina) {
        this.ataque = ataque;
        this.defensa = defensa;
        this.nombre = nombre;
        this.salud = salud;
        this.adrenalina = adrenalina;
    }

    @Override
    public void atacar(Personaje oponente) {
        if (adrenalina == 0) {
            oponente.recibirAtaque(ataque);
        } else {
            oponente.recibirAtaque(ataque * adrenalina);
        }
    }

    @Override
    public void recibirAtaque(int ataque) {
        this.salud -= ataque - defensa;
        if (this.salud < 0) {
            this.salud = 0;
        }
        if (this.adrenalina > 0) {
            this.ataque *= this.adrenalina;
        }
        if (this.adrenalina > 0) {
            this.adrenalina--;
        }
    }

    @Override
    public int getEspecial() {
        return this.adrenalina;
    }

    @Override
    public String toString() {
        return super.toString() + "[Ataque=" + ataque + "][Adrenalina=" + adrenalina + "]";
    }

}
