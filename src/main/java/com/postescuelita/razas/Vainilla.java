package com.postescuelita.razas;

public class Vainilla extends Personaje {

    public Vainilla(String nombre, int salud, int ataque, int defensa) {
        this.ataque = ataque;
        this.defensa = defensa;
        this.nombre = nombre;
        this.salud = salud;
    }

    @Override
    public void atacar(Personaje oponente) {
        oponente.recibirAtaque(ataque);
    }

    @Override
    public void recibirAtaque(int ataque) {
        this.salud -= ataque;
        if (this.salud < 0) {
            this.salud = 0;
        }
    }

    @Override
    public int getEspecial() {
        return 0;
    }

}
